window.onload = function() {
    $(".button-collapse").sideNav();
    var messages = [];
    var socket = io.connect('http://localhost:3700');
    var field = document.getElementById("field");
    var sendButton = document.getElementById("send");
    var content = document.getElementById("content");
    var name = document.getElementById("name");
    socket.on('message', function (data) {
        if(data.message) {
            var date = new Date(data.date);
            messages.push(data);
            var html = '';
            for(var i=0; i<messages.length; i++) {
                html += '<b>[' + (date.getHours() < 10 ? '0' + date.getHours() : date.getHours())
                            + ':' +
                            (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + '] '
                            + (messages[i].username ? messages[i].username : 'Server') + ' escribió: </b>';
                html += messages[i].message + '<br>';
            }
            content.innerHTML = html;
            content.scrollTop = content.scrollHeight;
        } else {
            console.log("There is a problem:", data);
        }
    });

    $(document).ready(function() {
        $("#field").keyup(function(e) {
            if(e.keyCode == 13) {
                sendMessage();
            }
        });
    });
 
    sendButton.onclick = sendMessage = function() {
        if(name.value == "") {
            alert("Please type your name!");
        } else {
            var text = field.value;
            socket.emit('send', { message: text, username: name.value });
            field.value = "";
        }
    };
 
}