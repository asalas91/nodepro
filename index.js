var express = require("express");
var app = express();
var port = 3700;
 
app.set('views', __dirname + '/tpl');
app.set('view engine', "jade");
app.engine('jade', require('jade').__express);
/* Basic
*app.get("/", function(req, res){
    res.send("It works!");
});*/
app.get("/", function(req, res){
    res.render("page");
});

app.use(express.static(__dirname + '/public'));

// Basic
//app.listen(port);
var io = require('socket.io').listen(app.listen(port));
//console.log("Listening on port " + port); Mensaje por consola

io.sockets.on('connection', function (socket) {
    socket.emit('message', { date: new Date() }); //Mensaje de inicio
    socket.on('send', function (data) {
    	console.log(data);
        io.sockets.emit('message', {date: new Date(), message: data.message, username: data.username});
    });
});

/*Excelente tutorial*/
//http://code.tutsplus.com/tutorials/real-time-chat-with-nodejs-socketio-and-expressjs--net-31708